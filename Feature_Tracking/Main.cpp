#include <conio.h>
#include <iostream>

#include <opencv\cv.h>
#include <opencv2\core.hpp>
#include <opencv\highgui.h>
#include <opencv2\opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;


int main(int argc, char** argv) {
	
	/*Initialize, load two images from the file system, and
	allocate the images and other structures we will need for
	results.*/

	Mat imgA = imread("img1.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat imgB = imread("img2.png", CV_LOAD_IMAGE_GRAYSCALE);

	Size img_sz = imgA.size();
	int win_size = 10;

	Mat imgC = imread("img2.png", CV_LOAD_IMAGE_UNCHANGED);

	/*The first thing we need to do is get the features
	we want to track.*/

	vector<Point2f> cornersA, cornersB;
	const int MAX_CORNERS = 500;
	goodFeaturesToTrack(imgA, cornersA, MAX_CORNERS, 0.01, 5, noArray(), 3, false, 0.04);

	cornerSubPix(imgA, cornersA, Size(win_size, win_size), Size(-1, -1),
		TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03));

	// Call the Lucas Kanade algorithm
	
	vector<uchar> features_found;
	calcOpticalFlowPyrLK(imgA, imgB, cornersA, cornersB, features_found, noArray(),
		Size(win_size * 4 + 1, win_size * 4 + 1), 5,
		TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, .3));

	// Now make some image of what we are looking at:

	for (int i = 0; i < (int)cornersA.size(); i++) {
		if (!features_found[i])
			continue;
		line(imgC, cornersA[i], cornersB[i], Scalar(0, 0, 255), 2, CV_AA);
	}

	namedWindow("ImageA", WINDOW_FREERATIO);
	imshow("ImageA", imgA);
	namedWindow("ImageB", WINDOW_FREERATIO);
	imshow("ImageB", imgB);
	namedWindow("LKpyr_OpticalFlow", WINDOW_FREERATIO);
	imshow("LKpyr_OpticalFlow", imgC);
	waitKey(0);

	return 0;
}